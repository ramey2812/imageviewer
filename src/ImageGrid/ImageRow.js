/**
 * Module Dependencies
 */
import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap';
import SingleImage from './SingleImage';
import './ImageRow.css';

/**
 * @class ImageRow that loads the image row
 */
class ImageRow extends Component {

    renderImages() {
        return this.props.images.map((image, index) =>(
            <Col md={3}><SingleImage key={index} image={image} /></Col>
        ));
    }
    render() {
        return  (
            <Row className="show-grid">
                {this.renderImages()}
            </Row>            
        );
    }
}

export default ImageRow;