/**
 * Module Dependencies
 */
import React, {Component} from 'react';
import {Grid} from 'react-bootstrap';
import ImageRow from './ImageRow';
import './ImageGrid.css';

/**
 * @class ImageGrid that loads the Image row
 */
class ImageGrid extends Component {
    renderRows() {
        var imageRows = [];
        var row = [];
        for (var i = 0; i < this.props.images.length; i++) {
            row.push(this.props.images[i]);
            // a row will have 4 images
            if (i  % 4 === 3) {
                imageRows.push(row);
                row = [];
            }
        }

        return imageRows.map((row, index) => (
            <ImageRow key={index} images={row} />
        ));
    }

    render() {        
        return (
            <Grid bsClass="image-grid">
                {this.renderRows()}
            </Grid>
        );
    }
}

export default ImageGrid;