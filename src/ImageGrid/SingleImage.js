/**
 * Module Dependencies
 */
import React, {Component} from 'react';
import {Modal, Button} from 'react-bootstrap';
import './SingleImage.css';

/**
 * @class SingleImage renders the image component.
 */
class SingleImage extends Component {

    constructor(props) {
        super(props);
        this.state = {showModal: false, modalFarm: '', modalServer: '', modalTitle: ''};
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
    }

    // handler to open the Modal
    open() {
        this.setState({showModal: true, modalFarm: this.props.image['$'].farm, modalServer: this.props.image['$'].server, modalTitle: this.props.image['$'].title});
    }

    // handler to close the Modal.
    close() {
        this.setState({showModal: false, modalFarm: '', modalServer: '', modalTitle: ''});
    }

    /**
     * @function renderUrl forms the image url
     * @return {string} url 
     */
    renderUrl() {
        var image = this.props.image;
        var imageFarm = image['$'].farm;
        var imageServer = image['$'].server;
        var imageId = image['$'].id;
        var imageSecret = image['$'].secret;
        var url = `https://farm${imageFarm}.staticflickr.com/${imageServer}/${imageId}_${imageSecret}.jpg`;
        return url;
    }

    render() {
        return (
            <div>
                <a onClick={this.open}><img src={this.renderUrl()} className="image" alt="" /></a>
                <Modal show={this.state.showModal} onHide={this.close}>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.modalTitle}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h6>Farm: </h6>
                        <p>{this.state.modalFarm}</p>
                        <hr/>
                        <h6>Server: </h6>
                        <p>{this.state.modalServer}</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.close}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default SingleImage;