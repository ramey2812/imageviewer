/**
 * @file
 * main file
 */

/**
 * Module Dependencies
 */

import React, {Component} from 'react';
import Navigation from './../Navigation/Navigation';
import ImageGrid from './../ImageGrid/ImageGrid';
import Xml2Js from 'xml2js';
import ScrollListener from 'react-scroll-listener';
import './App.css';

/**
 * @class App that loads ImageGrid and Navigation
 */
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {images: [], imagesToshow: [], lastImage: 12};
    this.doSearch = this.doSearch.bind(this);
    this.myScrollStartHandler = this.myScrollStartHandler.bind(this);
    this.scrollListener = new ScrollListener();
    this.scrollListener.addScrollHandler('scroller', this.myScrollStartHandler);
    this.scrollTimeout = null;
  }

  /**
   * @function myScrollHandler to handle scrolling and loading more images as user scrolls
   * @param {object} event 
   */
  myScrollStartHandler(event) {
    if (this.state.lastImage < this.state.images.length) {
      if (this.scrollTimeout != null) {
        clearTimeout(this.scrollTimeout);
      }
      // sets timeout of 300s after the user scrolls so that the scroll is complete
      this.scrollTimeout = setTimeout(() => {
        var lastImage = this.state.lastImage + 4;
        this.setState({ imagesToshow: this.state.images.slice(0, lastImage), lastImage: lastImage});
      }, 700);
    }
    
  }

  /**
   * @function doSearch to hit flickr api and get the images
   * @param {string} searchString 
   */
  doSearch(searchString) {
    var fetchUrl = `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=4bcde54cbf239cd4832a3332c78f2f69&text=${searchString}&content_type=1`;
    fetch(fetchUrl, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
      .then(response => {
        return response.text();
      })
      .then((data) => {
        Xml2Js.parseString(data, (err, string) => {
          var allImages = string.rsp.photos[0].photo;
          this.setState({images: allImages, imagesToshow: allImages.slice(0, this.state.lastImage)});
        })
      })
      .catch(err => alert(err));
  }

  render() {
    return (
      <div className='App'>
        <Navigation doSearch={this.doSearch}/>
        <ImageGrid images={this.state.imagesToshow}/>
      </div>
    );
  }
  
}

export default App;
