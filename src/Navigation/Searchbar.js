/**
 * Module Dependencies
 */
import React, {Component} from 'react';
import {Navbar, FormGroup, FormControl} from  'react-bootstrap';
import './Searchbar.css';

/**
 * @class Searchbar that renders the searchbar
 */
class Searchbar extends Component {
    constructor(props) {
        super(props);
        this.timeout = null;
        this.state = {searchString: ''};
        this.handleUpdate = this.handleUpdate.bind(this);
        this.addSearchString = this.addSearchString.bind(this);
    }

    /**
     * @function addSearchString adds the search string to query from flickr api
     */
    addSearchString() {
        this.props.addSearchString(this.state.searchString);
    }

    /**
     * @function handleUpdate updates the search string 
     * @param {object} event 
     */
    handleUpdate(event) {
        this.setState({searchString: event.target.value});
        if (this.timeout !== undefined) {
            clearTimeout(this.timeout);
        }
        // sets a timeout of 500 ms so the string is not updated as soon as it's typed.
        this.timeout = setTimeout(() => {
            this.addSearchString()
        }, 500)
    }
    render () {
        return (
            <Navbar.Form>
                <FormGroup >
                    <FormControl type="text" placeholder="Search" bsSize="small" bsClass="navBarSearchForm" onKeyUp={this.handleUpdate}/>
                </FormGroup>
            </Navbar.Form>
        );
    }
}

export default Searchbar;