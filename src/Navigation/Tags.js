/**
 * Module Dependencies
 */
import React, {Component} from 'react';
import Tag from './Tag';
import {Label} from 'react-bootstrap';

/**
 * @class Tags that display recent searches tags
 */
class Tags extends Component {
    constructor(props) {
        super(props);
        this.state = {tags: props.tags};
    }
    renderTags() {
        return this.state.tags.map((name, index) => (
            <Tag key={index} name={name}/>
        ));
    }
    render() {
        return (
            <div>
                <Label>Recent Searches: </Label>{this.renderTags()}
            </div>
        );
    }
}

export default Tags;