/**
 * Module Dependencies
 */
import React, { Component } from 'react';
import {Navbar} from 'react-bootstrap';
import { CookiesProvider, Cookies } from 'react-cookie';
import Searchbar from './Searchbar';
import Tags from './Tags';
import './Navigation.css';

/**
 * @class Navigation that renders the navigation bar,search bar and take care of the cookies
 */
class Navigation extends Component {
    constructor(props) {
        super(props);
        this.cookies = new Cookies();
        this.state = {searchString: '', searches: this.cookies.get('searches') || [] };
        this.addSearchString = this.addSearchString.bind(this);
    }

    /**
     * @function addSearchString adds the string to search and updates the cookies with new search
     * @param {string} newString 
     */
    addSearchString(newString) {
        if (this.state.searches.indexOf(newString) === -1 && newString !== '') {
            this.state.searches.push(newString);
            this.setState({searches: this.state.searches.slice(Math.max(this.state.searches.length - 5, 1))});
        }
        this.cookies.set('searches', this.state.searches);
        this.setState({searchString: newString});
        this.props.doSearch(this.state.searchString);
    }

    render() {
        return (
            <CookiesProvider>
                <div>
                    <Navbar fixedTop inverse>
                        <Navbar.Header>
                            <Navbar.Brand>
                                <a href="">Image Search</a>
                            </Navbar.Brand>
                        </Navbar.Header>
                        <Searchbar addSearchString={this.addSearchString}/>
                        <Tags tags={this.state.searches}/>
                    </Navbar>
                </div>
            </CookiesProvider>
        );
    }
}

export default Navigation;