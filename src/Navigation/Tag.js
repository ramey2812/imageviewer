/**
 * Module Dependencies
 */
import React, {Component} from 'react';
import {Label} from 'react-bootstrap';
import './Tag.css'

/**
 * @class Tag that displays the individual tag
 */
class Tag extends Component {
    constructor(props) {
        super(props);
        this.state = {name: props.name};
    }
    render() {
        return (
            <Label bsStyle="primary" bsClass="label">{this.state.name}</Label>
        );
    }
}

export default Tag;